//
//  ViewController.m
//  num8Auction
//
//  Created by steve on 2019/11/22.
//  Copyright © 2019 huashun. All rights reserved.
//

#import "ViewController.h"
#import <WebKit/WebKit.h>
#import "AppDelegate.h"

#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"


@interface ViewController ()<WKNavigationDelegate, WKUIDelegate, WKScriptMessageHandler>

//@property (nonatomic, strong) CLLocationManager * locationManager;
@property (nonatomic, strong) WKWebView                *webView;
@property (nonatomic, strong) UIActivityIndicatorView  *indicatorView;
@property (nonatomic, strong) AppDelegate              *appdelegate;

@end

@implementation ViewController

- (void) viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void) viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _appdelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    UIView *statusBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_Width, StatusBarHeight)];
    //    statusBarView.backgroundColor = RGB(34, 163, 252);
    statusBarView.backgroundColor= UIColor.whiteColor;
    [self.view addSubview:statusBarView];
    
    UIView *safeBottomSafeView = [[UIView alloc] initWithFrame:CGRectMake(0, SCREEN_Height - BottomSafeAreaHeight, SCREEN_Width, BottomSafeAreaHeight)];
    //    safeBottomSafeView.backgroundColor = RGB(34, 163, 252);
    safeBottomSafeView.backgroundColor = UIColor.whiteColor;
    [self.view addSubview:safeBottomSafeView];
    
    //设置监听
    WKUserContentController* userContentController = [[WKUserContentController alloc] init];
    [userContentController addScriptMessageHandler:self name:@"app"];
    
    // 设置偏好设置
    WKWebViewConfiguration *config = [[WKWebViewConfiguration alloc] init];
    config.userContentController = userContentController;
    
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(0, StatusBarHeight,  SCREEN_Width, SCREEN_Height - StatusBarHeight - BottomSafeAreaHeight) configuration:config];
    //    if (@available(iOS 11.0, *)) {
    //        self.webView.scrollView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    //    } else {
    //        self.automaticallyAdjustsScrollViewInsets = NO;
    //    }
    
    _webView.scrollView.bounces = YES;
    _webView.navigationDelegate = self;
    _webView.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:_webView];
    
    [self loadRequest];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkNetwork) name:NetWorkNotification object:nil]; //编辑用户设置
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getOrderPayResult:) name:@"WXPay" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getOrderPayResult:) name:@"AliPay" object:nil];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(getOauthResult:) name:OauthNotification object:nil];
}

- (void) checkNetwork {
    
    NetworkStatus status = _appdelegate.reachability.currentReachabilityStatus;
    if (status == NotReachable) {
        [self tipNoNetwork];
    } else {
        [self loadRequest];
    }
}

- (void) loadRequest {
    
    //页面加载逻辑
    //    NSString* urlString;
    //    urlString = URL_Address;
    //
    //    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:nil];
    //    request.timeoutInterval = 60.0f;
    //
    //    [_webView loadRequest:request];
    
    //页面加载逻辑
    NSString* urlString;
    NSString *userToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"userToken"];
    if (userToken) {
        urlString = [NSString stringWithFormat:@"%@?userId=%@",URL_Address,userToken];
    } else {
        urlString = URL_Address;
    }
    
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:@"GET" URLString:urlString parameters:nil error:nil];
    request.timeoutInterval = 60.0f;
    
    [_webView loadRequest:request];
    
}

- (void) tipNoNetwork {
    //    UIAlertView *alterView = [[UIAlertView alloc] initWithTitle:nil message:@"网络已断开，请检查网络！" delegate:nil cancelButtonTitle:@"知道了" otherButtonTitles:nil, nil];
    //    [alterView show];
}

- (void)getOrderPayResult:(NSNotification *)notification {
    
    NSString *result = notification.object;
    if ([result isEqualToString:@"success"]) {
        
        [self.webView evaluateJavaScript: [NSString stringWithFormat:@"paySuccess()"] completionHandler:^(id response, NSError * error) {
            
            NSLog(@"response %@ error %@",response, error);
            
        }];
    } else if ([result isEqualToString:@"cancel"]) {
        
        [self.webView evaluateJavaScript: [NSString stringWithFormat:@"payCancel()"] completionHandler:^(id response, NSError * error) {
            
            NSLog(@"response %@ error %@",response, error);
            
        }];
    } else if ([result isEqualToString:@"failed"]) {
        
        [self.webView evaluateJavaScript: [NSString stringWithFormat:@"payFailed()"] completionHandler:^(id response, NSError * error) {
            
            NSLog(@"response %@ error %@",response, error);
            
        }];
    }
}

- (void)getOauthResult:(NSNotification *)notification {
    NSDictionary *getDic = notification.object;
    
    NSLog(@"getDic %@",getDic);
    
    [self.webView evaluateJavaScript: [NSString stringWithFormat:@"wxLogin('%@')",getDic[@"code"]] completionHandler:^(id response, NSError * error) {
    //                NSLog(@"response: %@, \nerror: %@", response, error);
    }];
    
    
}


#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    
    NSDictionary *dic = message.body;
    NSLog(@"%@",dic[@"function"]);
    
    //    {
    //        content = 10022;
    //        function = getYijiawangUserId;
    //    }；
    
    if ([dic[@"function"] isEqualToString:@"getYijiawangUserId"] ) {
        [[NSUserDefaults standardUserDefaults] setObject:dic[@"content"] forKey:@"userToken"];
    }
    
    if ([dic[@"function"] isEqualToString:@"weiPay"] ) {
        
        NSString *payparamsString = [dic objectForKey:@"payparams"];
        
        NSDictionary *payparamsDic = [self dictionaryWithJsonString:payparamsString];
        //        [NTDataTool dictionaryWithJsonString:payparamsString];
        
        PayReq *request = [[PayReq alloc] init];
        request.partnerId = payparamsDic[@"partnerId"];
        request.prepayId= payparamsDic[@"prepayId"];
        request.package = @"Sign=WXPay";
        request.nonceStr= payparamsDic[@"nonceStr"];
        request.timeStamp= [payparamsDic[@"timeStamp"] intValue];
        request.sign= payparamsDic[@"sign"];
        
        //        [WXApi sendReq:request completion:^(BOOL success) {
        //        }];
        
        if ([WXApi sendReq:request]) {
        } else {
//            [SVProgressHUD showErrorWithStatus:@"未安装微信"];
        }
    }
    
    
    if ([dic[@"function"] isEqualToString:@"aliPay"] ) {
        NSString *appScheme = @"AuctionAlipay";
        [[AlipaySDK defaultService] payOrder:dic[@"payparams"] fromScheme:appScheme callback:^(NSDictionary *resultDic) {
            NSLog(@"reslut = %@",resultDic);
        }];
    }
    
    //   token
    else if ([dic[@"function"] isEqualToString:@"logout"] ) {
        [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"userToken"];
    }
    
    else if ([dic[@"function"] isEqualToString:@"thirdLoginWechat"] ) {
        
        SendAuthReq *req = [[SendAuthReq alloc] init];
//        req.scope = dic[@"appid"];
        req.scope = @"snsapi_userinfo";
//        req.state = dic[@"function"];
        //第三方向微信终端发送一个SendAuthReq消息结构
        [WXApi sendReq:req];
    }
    
}

#pragma mark-- 加载wkwebview 用于推送数据
- (void)loadWebView:(NSString *)urlString {
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
    [self.indicatorView startAnimating];
}

//如果不实现这个代理方法,默认会屏蔽掉打电话等url
- (void) webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    decisionHandler(WKNavigationActionPolicyAllow);
}

/// 2 页面开始加载
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation{
    
    [self.indicatorView startAnimating];
    
    //关于拨打电话时的调用问题
    NSString *path= [webView.URL absoluteString];
    NSString * newPath = [path lowercaseString];
    
    if ([newPath hasPrefix:@"sms:"] || [newPath hasPrefix:@"tel:"]) {
        UIApplication * app = [UIApplication sharedApplication];
        if ([app canOpenURL:[NSURL URLWithString:newPath]]) {
            [app openURL:[NSURL URLWithString:newPath]];
        }
        [self.indicatorView stopAnimating];
        return;
    }
}

/// 4 开始获取到网页内容时返回
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation {
}

/// 5 页面加载完成之后调用
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation{
    [self.indicatorView stopAnimating];
}

/// 页面加载失败时调用
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation{
    [self.indicatorView stopAnimating];
}


#pragma lazy
- (UIActivityIndicatorView *)indicatorView {
    if (!_indicatorView) {
        _indicatorView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0, 0, 20, 20)];
        _indicatorView.center = self.view.center;
        _indicatorView.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [self.view addSubview:_indicatorView];
    }
    return _indicatorView;
}

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
    if (jsonString == nil) {
        return nil;
    }
    
    NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
                                                        options:NSJSONReadingMutableContainers
                                                          error:&err];
    if(err) {
        NSLog(@"json解析失败：%@",err);
        return nil;
    }
    return dic;
}



@end
