//
//  AppDelegate.h
//  num8Auction
//
//  Created by steve on 2019/11/22.
//  Copyright © 2019 huashun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) Reachability *reachability;

@end

