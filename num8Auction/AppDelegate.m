//
//  AppDelegate.m
//  num8Auction
//
//  Created by steve on 2019/11/22.
//  Copyright © 2019 huashun. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"

#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"


@interface AppDelegate ()<WXApiDelegate>

@property (nonatomic, strong) ViewController *rootViewController;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
//    [WXApi registerApp:WX_APP_KEY universalLink:@""];
    
    [WXApi registerApp:WX_APP_KEY enableMTA:YES];

    //向微信注册支持的文件类型
    UInt64 typeFlag = MMAPP_SUPPORT_TEXT | MMAPP_SUPPORT_PICTURE | MMAPP_SUPPORT_LOCATION | MMAPP_SUPPORT_VIDEO |MMAPP_SUPPORT_AUDIO | MMAPP_SUPPORT_WEBPAGE | MMAPP_SUPPORT_DOC | MMAPP_SUPPORT_DOCX | MMAPP_SUPPORT_PPT | MMAPP_SUPPORT_PPTX | MMAPP_SUPPORT_XLS | MMAPP_SUPPORT_XLSX | MMAPP_SUPPORT_PDF;

    [WXApi registerAppSupportContentFlag:typeFlag];
    
    
    self.reachability = [Reachability reachabilityWithHostname:@"http://www.baidu.com"];
    
    self.reachability.reachableBlock = ^(Reachability *reachability) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:NetWorkNotification object:nil userInfo:nil];
        });
        
    };
    
    self.reachability.unreachableBlock = ^(Reachability *reachability) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:NetWorkNotification object:nil userInfo:nil];
        });
    };
    
    [self.reachability startNotifier];
    
    self.rootViewController = [[ViewController alloc] init];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:self.rootViewController];
    self.window.rootViewController = nav;
    [self.window makeKeyAndVisible];
    
    return YES;
}

// NOTE: 9.0以后使用新API接口
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString*, id> *)options {
    
    if ([url.host isEqualToString:@"safepay"]) { //跳转支付宝钱包进行支付，处理支付结果
        [[AlipaySDK defaultService] processOrderWithPaymentResult:url standbyCallback:^(NSDictionary *resultDic) { //通过回调判断支付成功
            if ([resultDic[@"resultStatus"]  isEqual: @"9000"]) { //支付成功，给出提示
                [[NSNotificationCenter defaultCenter] postNotificationName:@"AliPay" object:@"success"];
            } else if ([resultDic[@"resultStatus"]  isEqual: @"6001"]) {
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"AliPay" object:@"cancel"];
            } else  {
                 [[NSNotificationCenter defaultCenter] postNotificationName:@"AliPay" object:@"failed"];
            }
        }];
        return YES;
    } else if ([url.host isEqualToString:@"oauth"]) {
        NSDictionary *getDic = [self getUrlParameterWithUrl:url];
        [[NSNotificationCenter defaultCenter] postNotificationName:OauthNotification object:getDic];
        
    }else {
        return [WXApi handleOpenURL:url delegate:self];
    }
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void) onResp:(BaseResp*)resp {
    
    if ([resp isKindOfClass:[PayResp class]]) {
        
        NSString *wxPayResult = @"";
        
        PayResp *response = (PayResp *)resp;
        
        switch (response.errCode) {
            case WXSuccess://支付成功
                wxPayResult = @"success";
                break;
                
            case WXErrCodeUserCancel://支付取消
                wxPayResult = @"cancel";
                break;
                
            default:
                wxPayResult = @"failed";
                break;
        }
        
        NSNotification *notification = [NSNotification notificationWithName:@"WXPay" object:wxPayResult];
        [[NSNotificationCenter defaultCenter]postNotification:notification];
    }
}

- (NSDictionary *)getUrlParameterWithUrl:(NSURL *)url {
    NSMutableDictionary *parm = [[NSMutableDictionary alloc]init];
    //传入url创建url组件类
    NSURLComponents *urlComponents = [[NSURLComponents alloc] initWithString:url.absoluteString];
    //回调遍历所有参数，添加入字典
    [urlComponents.queryItems enumerateObjectsUsingBlock:^(NSURLQueryItem * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [parm setObject:obj.value forKey:obj.name];
    }];
    return parm;
}


@end
